import gspread

from google.oauth2 import service_account

from oauth2client.service_account import ServiceAccountCredentials

from odoo import models, fields, api

from xmlrpc import client

import os



def main():



 def odoo_login(odoo_url, odoo_db, odoo_username, odoo_password):

    common = client.ServerProxy('{}/xmlrpc/2/common'.format(odoo_url))

    uid = common.authenticate(odoo_db, odoo_username, odoo_password, {})

    models = client.ServerProxy('{}/xmlrpc/2/object'.format(odoo_url))

    return uid, models



    # Authentification Odoo

 odoo_url = 'http://127.0.0.1:8069'

 odoo_db = 'name'

 odoo_username = 'degentekle@mailfence.com'

 odoo_password = #It is private 



    # Connexion à Google Sheets

# Nom du fichier de clé JSON

 KEY_FILE = 'credentials.json'



# Chemin complet vers le fichier de clé JSON

 KEY_FILE_PATH = os.path.join(os.path.dirname(__file__), KEY_FILE)



# ID de la feuille de calcul Google Sheets

 SHEET_ID = '1XZNTErRE2aCLODf9JD9IBph6fmyp1nDtfQw2zIc8ycM'



# Portée OAuth pour accéder à Google Sheets

 SCOPE = ['https://www.googleapis.com/auth/spreadsheets'] 



 try:
    # Authentification avec le fichier de clé JSON

    credentials = service_account.Credentials.from_service_account_file(KEY_FILE, scopes=SCOPE)

    gc = gspread.authorize(credentials)

    

    # Ouverture de la feuille de calcul

    sheet = gc.open_by_key(SHEET_ID)

  



    # Sélection de la première feuille de calcul

    worksheet = sheet.get_worksheet(0)

    

    # Récupération des données

    data = worksheet.get_all_values()



    # Création de la liste products_data

    products_data = []

    for row in data:

    # Extraction des valeurs de chaque colonne

     name = row[0]

     list_price = row[1]

#e.g.standard_price = row[2]

#    ....

    ####--------------------------------------#### ici se trouve ce qu'on peut customiser 



    # Création d'un dictionnaire pour chaque produit

     product_data = {'name': name, 'list_price': list_price} #...'standard_price':standard_price}



    # Ajout du dictionnaire à la liste products_data

     products_data.append(product_data)



 except Exception as e:

    print("Erreur lors de la connexion à Google Sheets :", e)

# Connexion à Odoo

 odoo = odoo_login(odoo_url, odoo_db, odoo_username, odoo_password)



    # Récupération des données depuis Odoo

 def fetch_products(odoo_url, odoo_db, odoo_username, odoo_password):

    try:

        # Connexion à Odoo

        uid, models = odoo_login(odoo_url, odoo_db, odoo_username, odoo_password)



        # Récupération des produits

        product_ids = models.execute_kw(odoo_db, uid, odoo_password,

            'product.product', 'search', [[]])



        # Récupération des données des produits

        products = models.execute_kw(odoo_db, uid, odoo_password,

            'product.product', 'read', [product_ids])



        return products



    except Exception as e:

        print("Erreur lors de la récupération des produits depuis Odoo :", e)

        return []



# products = fetch_products(odoo_url, odoo_db, odoo_username, odoo_password)







 def update_products(odoo_url, odoo_db, odoo_username, odoo_password, products_data):

    try:

        # Connexion à Odoo

        uid, models = odoo_login(odoo_url, odoo_db, odoo_username, odoo_password)



        # Récupération des produits depuis Odoo

        product_ids = models.execute_kw(odoo_db, uid, odoo_password, 'product.template', 'search', [[]])

        products = models.execute_kw(odoo_db, uid, odoo_password, 'product.template', 'read', [product_ids])

        # Mise à jour des produits avec les données récupérées depuis Google Sheets

        for product_data in products_data:

            product_id = None

            for product in products:

                if product['name'] == product_data['name']:  # Supposons que le nom du produit est utilisé comme identifiant

                    product_id = product['id']

                    break

            if product_id:

                # Mettre à jour les valeurs des champs du produit

                models.execute_kw(odoo_db, uid, odoo_password, 'product.template', 'write', [[product_id], product_data])

            else:

                print("Le produit '{}' n'a pas été trouvé dans Odoo.".format(product_data['name']))



        print("Rafraichissement")

    except Exception as e:

        print("Erreur lors de la mise à jour des produits dans Odoo :", e)



# Utilisation de la fonction pour mettre à jour les produits

 update_products(odoo_url, odoo_db, odoo_username, odoo_password, products_data)







if __name__ == "__main__":

    main()