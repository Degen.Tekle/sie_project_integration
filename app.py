from prompt_toolkit import prompt

from prompt_toolkit.shortcuts import ProgressBar

import subprocess

import sys

import time





def run_script():

    try:

        # Exécutez votre script Python ici

        subprocess.run([sys.executable, "projet1.py"])

    except Exception as e:

        print("Erreur lors de l'exécution du script :", e)



def main():

    print("Bienvenue dans l'application d'exécution de script.")





    while True:

        print("\nExécution du script...")

        run_script()

        print("Script exécuté avec succès ")



        # Pause de 2 minutes

        time.sleep(2)







if __name__ == '__main__':

    main()

